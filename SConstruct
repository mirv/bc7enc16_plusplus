import os
import logging
import multiprocessing

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)

# Set -j option to be the cpu count by default.
SetOption("num_jobs", multiprocessing.cpu_count())
logging.info("building with -j" + str(GetOption("num_jobs")))
# Cache the checksum if the file is older than 8 seconds.
SetOption("max_drift", 8)

# default to release build
buildmode = ARGUMENTS.get('mode', 'debug')

AddOption("--sanitise",
          dest="sanitise",
          nargs=1,
          type="int",
          action="store",
          metavar="1",
          help="Compile with sanitiser checks enabled (1), or disabled (0)",
          default=0)

env = Environment(ENV = os.environ)


env.Append(CCFLAGS = ['-Wall', '-Wpedantic', '-march=native', '-std=c++20', '-fPIC'])
env.Append(LIBS = ['pthread'])

builddir = "build"
if buildmode == 'debug' :
    logging.info("debug build active")
    env.Append(CCFLAGS = ["-O0",
                          "-ggdb",
                          "-fPIC"])
    env.Append(CPPDEFINES = ['DEBUG'])
    builddir = os.path.join("build", "debug")
else :
    logging.info("release build active")
    env.Append(CCFLAGS = ["-O2",
                          "-fPIC",
                          "-fomit-frame-pointer",
                          "-fexceptions" ])
    env.Append(CPPDEFINES = ['RELEASE'])
    builddir = os.path.join("build", "release")

env_vars = {
    "buildmode" : buildmode,
    "builddir" : builddir,
    "env": env
}

SConscript("src/SConscript",
           variant_dir=builddir,
           duplicate=0,
           exports=env_vars)

# Delete everything. Removes the build folder entirely.
env.Command("nuke", None, Delete("build"))
