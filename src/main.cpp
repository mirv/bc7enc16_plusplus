
// Clang isn't guaranteed to provide C++20 at the time of writing.
#if defined(__clang__)
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#else
#include <filesystem>
namespace fs = std::filesystem;
#endif
#include <system_error>

#include <ranges>
#include <fstream>

#include "cxxopts.hpp"
#include "util.h"
#include "mThreadpond.h"

///< Input png file to convert.
static fs::path g_input_path;

///< Output ktx file to generate.
static fs::path g_output_path;

///< Local environment directory.
static fs::path g_base_path;

///< Task pool.
static mThreadpond g_thread_pond;
static mSemaphore g_semaphore;

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "bc7enc16.h"

static bc7enc16_compress_block_params g_pack_params;

/**
 * BC7 encoded texel data block (4x4 pixel block).
 */
struct BC7Block {
	uint64_t m_vals[2];
};

/**
 * Raw RGBA image container. Total storage space used is width * height * 4.
 */
struct Image {
	int width;        ///< Image width. Must be multiple of 4.
	int height;       ///< Image height. Must be multiple of 4.
	uint8_t *raw;     ///< Texel data, RGBA format.
	BC7Block *bc7_blocks;  ///< bc7 encoded texel blocks.
};

struct TexelHighP {
	double r;
	double g;
	double b;
	double a;
};

/**
 * Precision image container. Useful to not lose precision when downsampling.
 */
struct ImageHighP {
	int width;
	int height;
	TexelHighP *texels;
};

///< Master image used for downsampling. Global variable to help with multithreading.
static ImageHighP g_highp_image = {};

///< Array of images, each successive image being the next lower (in size) mip map image.
static std::vector<Image> g_images;

///< Number of mipmap levels to support. 0 for as many as possible.
static int g_mipmap_levels = 0;

void parse_options(int argc, char *argv[])
{
#ifndef WIN32  // *nix

	std::error_code err;
	fs::path self_path = fs::read_symlink("/proc/self/exe", err);
	if (err) {
		// Failed to read the symlink, or it doesn't exist. Fall back to argv[0] dir path.
		self_path = argv[0];
	}
	g_base_path = self_path.parent_path();

#else  // WIN32

	char *path = new char[4096];
	GetModuleFileName(GetModuleHandle(NULL), path, 4096);
	char *dirpath = strrchr(path, '\\');
	if(dirpath != NULL)
		*dirpath = 0;
	base_path = path;
	delete [] path;

#endif

	try {
		cxxopts::Options options(argv[0], ".png to .ktx BC7 encoder");
		options
			.positional_help("[optional args]")
			.show_positional_help();

		options
			.allow_unrecognised_options()
			.add_options()
			("i,input", "input *.png file", cxxopts::value<std::string>())
			("o,output", "output *.ktx file", cxxopts::value<std::string>())
			("l,levels", "number of mipmaps to generate", cxxopts::value<int>())
			("h,help", "print help message")
			;

		auto result = options.parse(argc, argv);

		if (result.count("help")) {
			std::cout << options.help({""}) << std::endl;
			std::exit(0);
		}

		if (result.count("input")) {
			g_input_path = result["input"].as<std::string>();
		} else {
			ERROR_LOG("no input file specified.\n");
			std::terminate();
		}

		if (result.count("output")) {
			g_output_path = result["output"].as<std::string>();
		} else {
			ERROR_LOG("no output file specified.\n");
			std::terminate();
		}

		if (result.count("levels")) {
			g_mipmap_levels = result["levels"].as<int>();
		}

	} catch (const cxxopts::OptionException &e) {
		ERROR_LOG("parse options failure: {}\n", e.what());
		std::terminate();
	}
}

void sub_encode(int index, int start, int count)
{
	Image &image = g_images[index];

	int blocks_x = image.width / 4;
	int stop = start + count;

	for (int by = start; by < stop; by++) {
		for (int bx = 0; bx < blocks_x; bx++) {
			uint32_t texels[16];
			for (int i = 0; i < 4; ++i) {
				int offset = 4 * (((by * 4) + i) * image.width) + (bx * 16);
				memcpy(&texels[i*4], &image.raw[offset], 16);
			}
			bc7enc16_compress_block(&image.bc7_blocks[by * blocks_x + bx], texels, &g_pack_params);
		}
	}

	g_semaphore.decrement();
}

static TexelHighP *g_thread_downsamples;
void sub_downsample([[maybe_unused]] int index, int start, int count)
{
	int width = g_highp_image.width / 2;
	int stop = start + count;

	for (int h = start; h < stop; ++h) {
		for (int w = 0; w < width; ++w) {
			TexelHighP a = g_highp_image.texels[((h*2)+0) * g_highp_image.width + ((w*2) + 0)];
			TexelHighP b = g_highp_image.texels[((h*2)+1) * g_highp_image.width + ((w*2) + 0)];
			TexelHighP c = g_highp_image.texels[((h*2)+0) * g_highp_image.width + ((w*2) + 1)];
			TexelHighP d = g_highp_image.texels[((h*2)+1) * g_highp_image.width + ((w*2) + 1)];

			g_thread_downsamples[h*width + w] = {
				(a.r + b.r + c.r + d.r) / 4.0,
				(a.g + b.g + c.g + d.g) / 4.0,
				(a.b + b.b + c.b + d.b) / 4.0,
				(a.a + b.a + c.a + d.a) / 4.0,
			};
		}
	}
	g_semaphore.decrement();
}

void highp_to_uint8(ImageHighP &src, Image &dst)
{
	dst.width = src.width;
	dst.height = src.height;
	dst.raw = new uint8_t[src.width * src.height * 4];
	for (int h = 0; h < src.height; ++h) {
		int offset_h = h * src.width;
		for (int w = 0; w < src.width; ++w) {
			int offset = offset_h + w;
			dst.raw[offset * 4 + 0] = static_cast<uint8_t>(src.texels[offset].r + 0.5);
			dst.raw[offset * 4 + 1] = static_cast<uint8_t>(src.texels[offset].g + 0.5);
			dst.raw[offset * 4 + 2] = static_cast<uint8_t>(src.texels[offset].b + 0.5);
			dst.raw[offset * 4 + 3] = static_cast<uint8_t>(src.texels[offset].a + 0.5);
		}
	}
}

void uint8_to_highp(Image &src, ImageHighP &dst)
{
	dst.width = src.width;
	dst.height = src.height;
	dst.texels = new TexelHighP[src.width * src.height];
	for (int h = 0; h < src.height; ++h) {
		int offset_h = h * src.width;
		for (int w = 0; w < src.width; ++w) {
			int offset = offset_h + w;
			dst.texels[offset].r = static_cast<double>(src.raw[offset * 4 + 0]);
			dst.texels[offset].g = static_cast<double>(src.raw[offset * 4 + 1]);
			dst.texels[offset].b = static_cast<double>(src.raw[offset * 4 + 2]);
			dst.texels[offset].a = static_cast<double>(src.raw[offset * 4 + 3]);
		}
	}
}

bool load_png(const fs::path &filepath)
{
	int width, height, channels;
	uint8_t* pixels = stbi_load(filepath.c_str(),
	                            &width,
	                            &height,
	                            &channels,
	                            STBI_rgb_alpha);

	if (pixels == nullptr) {
		return false;  // Failed to load image (for whatever reason).
	}

	if ((width <= 0) || (height <= 0)) {
		stbi_image_free(pixels);
		return false;
	}

	if ( (static_cast<unsigned int>(width) & 0x03U) ||
	     (static_cast<unsigned int>(height) & 0x03U)) {
		ERROR_LOG("Image dimensions are not a multiple of 4, cannot compress.\n");
		return false;
	}

	/**
	 * Actually most use cases will have source files already as a multiple of 4, and
	 * so the pixel data from stbi_load could be directly used. Making a copy is in
	 * these cases a complete waste of time and effort.
	 */

	Image image = {};
	image.raw = pixels;
	image.width = width;
	image.height = height;
	//stbi_image_free(pixels);

	// Convert to high precision format so downsampling is more accurate.
	uint8_to_highp(image, g_highp_image);
	g_images.push_back(image);


	int level_limit = g_mipmap_levels > 0 ? g_mipmap_levels : std::numeric_limits<int>::max();
	int level = 1;  // Base level (0) has already been done, so start at 1 here.

	// Now downsample to the lowest mipmap level.
	int downsample_width = g_highp_image.width;
	int downsample_height = g_highp_image.height;
	while ((downsample_width >= 8) && (downsample_height >= 8) && (level < level_limit)) {
		downsample_width >>= 1;
		downsample_height >>= 1;
		level++;

		// Need a new destination texel array to write to.
		g_thread_downsamples = new TexelHighP[downsample_width * downsample_height];
		/**
		 * Let's make this multithreaded as well, because why not. The math is pretty simple though
		 * and this may not actually gain much speed at the end of the day, especially if the
		 * threads sleep and have to be woken before the next round.
		 */
		if (int rows_per_thread = (downsample_height / g_thread_pond.threadsCount()); rows_per_thread > 0) {
			for (int i = 0; i < downsample_height; i+=rows_per_thread) {
				// Make sure to increment the semaphore _before_ a task is queued.
				g_semaphore.increment();
				if ((i + rows_per_thread) <= downsample_height) {
					g_thread_pond.queueTask(sub_downsample, 0, i, rows_per_thread);
				} else {
					g_thread_pond.queueTask(sub_downsample, 0, i, downsample_height - i);
				}
			}
		} else {
			g_semaphore.increment();
			g_thread_pond.queueTask(sub_downsample, 0, 0, downsample_height);
		}
		g_thread_pond.process();
		g_semaphore.wait();

		delete [] g_highp_image.texels;
		g_highp_image.texels = g_thread_downsamples;
		g_highp_image.width = downsample_width;
		g_highp_image.height = downsample_height;

		highp_to_uint8(g_highp_image, image);
		g_images.push_back(image);
	}
	delete [] g_highp_image.texels;

	// Queue all images for encoding.
	for (int index = 0; index < static_cast<int>(g_images.size()); ++index) {
		int blocks_x = g_images[index].width / 4;
		int blocks_y = g_images[index].height / 4;
		g_images[index].bc7_blocks = new BC7Block[blocks_x * blocks_y];

		/**
		 * Queue rows of the image for encoding. To keep the thread task queuing overhead to a
		 * minimum, try to queue as few tasks as possible. To achieve that, try to even out by
		 * assigning an equal number of rows to each thread, with any left over being given as
		 * an extra task of minor impact.
		 */
		if (int rows_per_thread = blocks_y / g_thread_pond.threadsCount(); rows_per_thread > 0) {
			for (int i = 0; i < blocks_y; i+=rows_per_thread) {
				// Make sure to increment the semaphore _before_ a task is queued.
				g_semaphore.increment();
				if ((i + rows_per_thread) <= blocks_y) {
					g_thread_pond.queueTask(sub_encode, index, i, rows_per_thread);
				} else {
					g_thread_pond.queueTask(sub_encode, index, i, blocks_y - i);
				}
			}
		} else {
			// May cores compared to the number of blocks. Just do the lot in one go.
			g_semaphore.increment();
			g_thread_pond.queueTask(sub_encode, index, 0, blocks_y);
		}
	}
	g_thread_pond.process();
	g_semaphore.wait();

	return true;
}

static bool save_bc7_ktx2(const fs::path &filepath, bool srgb)
{
	std::ofstream outfile;

	outfile.open(filepath, std::ofstream::binary);
	if (outfile.bad()) {
		outfile.close();
		ERROR_LOG("Failed to create file {}\n", filepath.string());
	}

	const uint8_t FileIdentifier[12] = {
		0xAB, 0x4B, 0x54, 0x58, 0x20, 0x32, 0x30, 0xBB, 0x0D, 0x0A, 0x1A, 0x0A
	};


	size_t total_image_size = 0;
	for (const auto &image : g_images) {
		total_image_size += image.width * image.height;
	}

	outfile.write(reinterpret_cast<const char *>(FileIdentifier), 12);

	//VK_FORMAT_BC7_UNORM_BLOCK = 145,
	//VK_FORMAT_BC7_SRGB_BLOCK = 146,
	outfile << static_cast<uint32_t>(145);                // vkFormat
	outfile << static_cast<uint32_t>(8);                  // typeSize
	outfile << static_cast<uint32_t>(g_images[0].width);  // pixelWidth
	outfile << static_cast<uint32_t>(g_images[0].height); // pixelHeight
	outfile << static_cast<uint32_t>(0U);                 // pixelDepth
	outfile << static_cast<uint32_t>(0U);                 // layerCount (array textures)
	outfile << static_cast<uint32_t>(1U);                 // faceCount
	outfile << static_cast<uint32_t>(g_images.size());    // levelCount
	outfile << static_cast<uint32_t>(0U);                 // supercompressionScheme
	outfile << static_cast<uint32_t>(80 + g_images.size() * 24); // dfdByteOffset
	outfile << static_cast<uint32_t>(4U);                 // dfdByteLength
	outfile << static_cast<uint32_t>(0U);                 // kvdByteOffset
	outfile << static_cast<uint32_t>(0U);                 // kvdByteLength
	outfile << static_cast<uint64_t>(0U);                 // sgdByteOffset
	outfile << static_cast<uint64_t>(0U);                 // sgdByteLength

	// 80 bytes to here.

	size_t mipmap_offset = total_image_size + 80 + (g_images.size() * 24) + 4;
	for (const auto &image : g_images) {
		mipmap_offset -= image.width * image.height;
		outfile << static_cast<uint64_t>(mipmap_offset);               // Byte offset (from file start).
		outfile << static_cast<uint64_t>(image.width * image.height);  // Byte length, which is 1/4 of RGBA requirements.
		outfile << static_cast<uint64_t>(image.width * image.height);  // Uncompressed byte length
	}

	// 80 bytes + (g_images.size() * 24) to here.

	outfile << static_cast<uint32_t>(4U);                 // DfdTotalSize (including this field).

	// 80 bytes + (g_images.size() * 24) + 4 to here.

	// ktx2 mandates that the smallest mipmap is stored first.
	for (const auto &image : g_images | std::ranges::views::reverse) {
		outfile.write(reinterpret_cast<const char *>(image.bc7_blocks), image.width * image.height);
	}

	outfile.flush();
	outfile.close();

	return true;

#if 0
	FILE *pFile = NULL;
	pFile = fopen(filepath.c_str(), "wb");
	if (!pFile) {
		ERROR_LOG("Failed to create file {}\n", filepath.c_str());
		return false;
	}

	uint32_t pixel_format_bpp = 8;
	uint32_t width = static_cast<uint32_t>(g_images[0].width);
	uint32_t height = static_cast<uint32_t>(g_images[0].height);

	unsigned char FileIdentifier[12] = {
		0xAB, 0x4B, 0x54, 0x58, 0x20, 0x31, 0x31, 0xBB, 0x0D, 0x0A, 0x1A, 0x0A
	};
	fwrite(FileIdentifier, 12, 1, pFile);

	uint32_t endianess = 0x04030201; // Endianess indicator
	uint32_t gl_type = 0;            // 0 for compressed files.
	uint32_t gl_type_size = 1;       // 1 for compressed files.
	uint32_t gl_format = 0;          // 0 for compressed files.
	uint32_t gl_internal_format = 0; // Unsure.
	uint32_t vk_format = 145;// VK_FORMAT_BC7_UNORM_BLOCK;  // Vulkan format.
	uint32_t pixel_width = width;
	uint32_t pixel_height = height;
	uint32_t pixel_depth = 0;
	uint32_t number_array_elements = 0;
	uint32_t number_faces = 1;       // 2D texture, one face.
	//uint32_t number_mip_levels = 1;  // Mipmap levels. Sort out multiple levels later.
	uint32_t number_mip_levels = static_cast<uint32_t>(g_images.size());  // Mipmap levels. Sort out multiple levels later.
	uint32_t level_order = 0;        // Mipmap level ordering. 0 means base level first.
	uint32_t supercompression_scheme = 0;
	uint32_t data_format_descriptor_size = 0;
	uint32_t key_value_data_size = 0;
	uint32_t supercompression_global_data_size = 0;
	uint32_t total_image_size = (((width + 3) & ~3) * ((height + 3) & ~3) * pixel_format_bpp) >> 3;
	uint32_t total_image_uncompressed_size = total_image_size;  // No supercompression, must be same as above.

	// For each mip level.
	uint32_t level_index = 0;

	// For each mip level (again).
	uint32_t miplevel_size = total_image_size;
	uint32_t miplevel_uncompressed_size = total_image_size; // No supercompression, must be same as above.

	fwrite(&endianess, 4, 1, pFile);
	fwrite(&gl_type, 4, 1, pFile);
	fwrite(&gl_type_size, 4, 1, pFile);
	fwrite(&gl_format, 4, 1, pFile);
	fwrite(&gl_internal_format, 4, 1, pFile);
	fwrite(&vk_format, 4, 1, pFile);
	fwrite(&pixel_width, 4, 1, pFile);
	fwrite(&pixel_height, 4, 1, pFile);
	fwrite(&pixel_depth, 4, 1, pFile);
	fwrite(&number_array_elements, 4, 1, pFile);
	fwrite(&number_faces, 4, 1, pFile);
	fwrite(&number_mip_levels, 4, 1, pFile);
	fwrite(&level_order, 4, 1, pFile);
	fwrite(&supercompression_scheme, 4, 1, pFile);
	fwrite(&data_format_descriptor_size, 4, 1, pFile);
	fwrite(&key_value_data_size, 4, 1, pFile);
	fwrite(&supercompression_global_data_size, 4, 1, pFile);
	fwrite(&total_image_size, 4, 1, pFile);
	fwrite(&total_image_uncompressed_size, 4, 1, pFile);

	for (uint32_t i = 0; i < g_images.size(); ++i) {
		fwrite(&i, 4, 1, pFile);
		miplevel_size = (((g_images[i].width + 3) & ~3) * ((g_images[i].height + 3) & ~3) * pixel_format_bpp) >> 3;
		fwrite(&miplevel_size, 4, 1, pFile);
		fwrite(&miplevel_size, 4, 1, pFile);  // No supercompression, so it's the same as above.
		fwrite(g_images[i].bc7_blocks, miplevel_size, 1, pFile);
	}


	if (fclose(pFile) == EOF) {
		ERROR_LOG("Failed writing to {}\n", filepath.c_str());
		return false;
	}
	return true;
#endif
}

int main(int argc, char **argv)
{
	/**
	 * First parse command line options and pull out a list of files and interesting filesystem
	 * paths. The program may not necessarily be run from the install directory, to current
	 * environment directory is important to know.
	 */
	parse_options(argc, argv);

	/**
	 * The basic idea is to load each .png in turn, converting to bc7 format, before storing
	 * all of the data to a single .ktx file. It's presumed that multiple input files basically
	 * means mipmap levels.
	 */

	bc7enc16_compress_block_init();

	bc7enc16_compress_block_params_init(&g_pack_params);
	/*
	if (!perceptual)
		bc7enc16_compress_block_params_init_linear_weights(&g_pack_params);
	g_pack_params.m_max_partitions_mode1 = max_partitions_to_scan;
	g_pack_params.m_uber_level = uber_level;
	*/

	g_thread_pond.start();

	if (load_png(g_input_path)) {
		save_bc7_ktx2(g_output_path, true);
	}

	for (auto &image : g_images) {
		delete [] image.raw;
		delete [] image.bc7_blocks;
	}
	g_images.clear();

	g_thread_pond.stop();

	return 0;
}
