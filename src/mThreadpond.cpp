#ifdef WIN32
#include <Windows.h>
#endif

#include "mThreadpond.h"
#include <time.h>

#ifndef WIN32
static timespec sleepy_time = {0, 1000000 };
#endif

thread_local unsigned int mThreadpond::m_thread_id;

mThreadpond::mThreadpond() :
	m_threads_count(0),
	m_threads_active(0),
	m_threads_idle(0),
	m_task_list(),
	m_thread_array(nullptr),
	m_thread_wait(true)
{
	mThreadpond::m_thread_id = 0;
}

mThreadpond::~mThreadpond()
{
	if (m_thread_array != nullptr) {
		unsigned int count = m_threads_count;
		m_threads_count = 0;
		//lock_guard<mutex> wait_lock(pool_idle_wait_mutex);
		m_cond_idle_wait.notify_all();

		//this->wait();       // Waits for any tasks to be completed.
		m_threads_count = 0;  // Shutdown any threads.
		for (unsigned int i = 0; i < count; i++) {
			m_thread_array[i]->join();
			delete m_thread_array[i];
		}
		delete [] m_thread_array;
	}
}

void mThreadpond::start()
{
	m_threads_count = std::thread::hardware_concurrency();
	m_threads_idle = m_threads_count;
	m_thread_array = new thread *[m_threads_count];
	for(unsigned int i = 0; i < m_threads_count; i++)
		m_thread_array[i] = new thread(ref(*this), i+1);
}

void mThreadpond::stop()
{
	unsigned int count = m_threads_count;
	m_threads_count = 0;
	//lock_guard<mutex> wait_lock(pool_idle_wait_mutex);
	m_cond_idle_wait.notify_all();

	//this->wait();       // Waits for any tasks to be completed.
	m_threads_count = 0;  // Shutdown any threads.
	for (unsigned int i = 0; i < count; i++) {
		m_thread_array[i]->join();
		delete m_thread_array[i];
	}
	delete [] m_thread_array;
	m_thread_array = nullptr;

	m_threads_idle = 0;
}

void mThreadpond::operator()(unsigned int id)  // Self-calling function for thread processing.
{
	uint32_t spinner = 10;
	mThreadpond::m_thread_id = id;

	//PondEntry entry;
	do {
		m_pool_mutex.lock();
		if (m_task_list.size() == 0) {
			m_pool_mutex.unlock();
			if (--spinner) {
#ifdef WIN32
				Sleep(1);
#else
				nanosleep(&sleepy_time, NULL);
#endif
			} else {
				spinner = 10;
				unique_lock<mutex>wait_lock(m_pool_idle_wait_mutex);
				m_cond_idle_wait.wait(wait_lock);
			}

		} else {
			PondEntry entry(m_task_list.front());
			m_task_list.pop();
			m_threads_idle--;
			m_threads_active++;
			m_pool_mutex.unlock();

			entry.cb(entry.index, entry.start, entry.count);

			m_pool_mutex.lock();
			m_threads_idle++;
			m_threads_active--;
			m_pool_mutex.unlock();

			spinner = 10;
		}
	}while(m_threads_count);
}

void mThreadpond::process()
{
	m_pool_mutex.lock();
	while (m_task_list.size() > 0) {
		PondEntry entry(m_task_list.front());
		m_task_list.pop();
		m_pool_mutex.unlock();
		entry.cb(entry.index, entry.start, entry.count);
		m_pool_mutex.lock();
	}

	m_pool_mutex.unlock();
}

void mThreadpond::notifyAll()
{
	lock_guard<mutex> wait_lock(m_pool_idle_wait_mutex);
	m_cond_idle_wait.notify_all();
}

void mThreadpond::notifyOne()
{
	lock_guard<mutex> wait_lock(m_pool_idle_wait_mutex);
	m_cond_idle_wait.notify_one();
}

void mThreadpond::queueTask(std::function<void(int, int, int)> cb,
                            int index,
                            int start,
                            int count,
                            bool bNotify)
{
	m_pool_mutex.lock();
	m_task_list.push({cb, index, start, count});
	m_pool_mutex.unlock();

	if (bNotify) {
		// Signal new task for any waiting thread to handle.
		lock_guard<mutex> wait_lock(m_pool_idle_wait_mutex);
		m_cond_idle_wait.notify_one();
	}
}
