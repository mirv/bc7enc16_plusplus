#ifndef UTIL_H
#define UTIL_H

#include <fmt/format.h>

// Set this to <source_location> and source = std when the compiler supports it.
#include <experimental/source_location>
namespace source = std::experimental;

namespace logger {

void debug(const char *format,
           fmt::format_args args,
           const source::source_location &location = source::source_location::current());

template<typename ...Args>
void tdebug(const source::source_location& location,
            const char *format,
            const Args & ... args) {
	logger::debug(format, fmt::make_format_args(args...), location);
}

void info(const char *format,
          fmt::format_args args,
          const source::source_location &location = source::source_location::current());

template<typename ...Args>
void tinfo(const source::source_location& location,
           const char *format,
           const Args & ... args)
{
	logger::info(format, fmt::make_format_args(args...), location);
}

void warning(const char *format,
             fmt::format_args args,
             const source::source_location &location = source::source_location::current());

template<typename ...Args>
void twarning(const source::source_location& location,
              const char *format,
              const Args & ... args)
{
	logger::warning(format, fmt::make_format_args(args...), location);
}

void error(const char *format,
           fmt::format_args args,
           const source::source_location &location = source::source_location::current());

template<typename ...Args>
void terror(const source::source_location& location,
            const char *format,
            const Args & ... args)
{
	logger::error(format, fmt::make_format_args(args...), location);
}

void todo(const char *format,
           fmt::format_args args,
           const source::source_location &location = source::source_location::current());

template<typename ...Args>
void ttodo(const source::source_location& location,
            const char *format,
            const Args & ... args)
{
	logger::todo(format, fmt::make_format_args(args...), location);
}


} // namespace logger

#define DEBUG_LOG(x, ...) logger::tdebug(source::source_location::current(), x, ##__VA_ARGS__)
#define INFO_LOG(x, ...) logger::tinfo(source::source_location::current(), x, ##__VA_ARGS__)
#define WARNING_LOG(x, ...) logger::twarning(source::source_location::current(), x, ##__VA_ARGS__)
#define ERROR_LOG(x, ...) logger::terror(source::source_location::current(), x, ##__VA_ARGS__)
#define TODO_LOG(x, ...) logger::ttodo(source::source_location::current(), x, ##__VA_ARGS__)

#endif // UTIL_H
