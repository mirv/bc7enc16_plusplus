#ifndef THREADPOND_H
#define THREADPOND_H

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <atomic>
using std::thread;
using std::mutex;
using std::condition_variable;
using std::ref;
using std::unique_lock;
using std::lock_guard;
using std::atomic;

class mThreadpond
{
protected:
	unsigned int m_threads_count;
	unsigned int m_threads_active;
	unsigned int m_threads_idle;

	struct PondEntry
	{
		std::function<void(int, int, int)> cb;
		int index;
		int start;
		int count;
	};

	std::queue<PondEntry>m_task_list;
	thread **m_thread_array;

	mutex m_pool_mutex;
	mutex m_pool_idle_mutex;

	mutex m_pool_idle_wait_mutex;
	condition_variable m_cond_idle_wait;

	static thread_local unsigned int m_thread_id;
public:

	mThreadpond();
	~mThreadpond();

	void start();

	void setThreadSpinLock(bool b_spin) { m_thread_wait = !b_spin; }
	bool getThreadSpinLock() { return !m_thread_wait; }

	void stop();

	void operator()(unsigned int id);  // Self-calling function for thread processing.
	void process();  // Blocking call that helps process all pond entries. Returns when the entry queue is empty.

	void notifyAll();
	void notifyOne();

	inline unsigned int threadsIdle() const volatile { return m_threads_idle; }
	inline unsigned int threadsActive() const volatile { return m_threads_active; }
	inline unsigned int threadsCount() const { return m_threads_count; }

	inline auto tasksCount() const { return m_task_list.size(); }

	static inline unsigned int threadID() { return mThreadpond::m_thread_id; }

	void queueTask(std::function<void(int, int, int)> cb,
	               int index,
	               int start,
	               int count,
	               bool bNotify = true);
};

class mSemaphore
{
private:
	unsigned int m_counter;
	mutex m_counter_mutex;
	condition_variable m_counter_cond;

public:
	mSemaphore() : m_counter(0) {}
	~mSemaphore() {}

	void increment()
	{
		m_counter_mutex.lock();
		m_counter++;
		m_counter_mutex.unlock();
	}

	void decrement()
	{
		m_counter_mutex.lock();
		m_counter--;
		if (m_counter == 0)
			m_counter_cond.notify_one();
		m_counter_mutex.unlock();
	}

	inline unsigned int count() const volatile { return m_counter; }

	void wait()  // Wait for condition (counter == 0).
	{
		unique_lock<mutex> lock(m_counter_mutex);
		while(count() > 0)
			m_counter_cond.wait(lock);
	}
};

class mSpinlock
{
private:
	atomic<unsigned int> m_counter;

public:
	mSpinlock() : m_counter(0) {}
	~mSpinlock() {}


	inline void increment()	{
		++m_counter;
	}

	inline void decrement() {
		--m_counter;
	}

	inline unsigned int count() const volatile { return m_counter; }

	///< Wait for condition (counter == 0).
	inline void wait() const {
		while(m_counter.load() > 0) {}
	}
};

#endif
