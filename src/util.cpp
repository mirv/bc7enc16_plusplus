#include "util.h"

namespace logger {

void debug(const char *format,
           fmt::format_args args,
           const std::experimental::source_location &location)
{
	fmt::print(stdout, "DEBUG {}({}): ", location.file_name(), location.line());
	fmt::vprint(stdout, format, args);
}

void info(const char *format,
          fmt::format_args args,
          const std::experimental::source_location &location)
{
	fmt::print(stdout, "INFO {}({}): ", location.file_name(), location.line());
	fmt::vprint(stdout, format, args);
}

void warning(const char *format,
             fmt::format_args args,
             const std::experimental::source_location &location)
{
	fmt::print(stdout, "WARNING {}({}): ", location.file_name(), location.line());
	fmt::vprint(stdout, format, args);
}

void error(const char *format,
           fmt::format_args args,
           const std::experimental::source_location &location)
{
	fmt::print(stdout, "ERROR {}({}): ", location.file_name(), location.line());
	fmt::vprint(stderr, format, args);
}

void todo(const char *format,
          fmt::format_args args,
          const std::experimental::source_location &location)
{
	fmt::print(stdout, "TODO {}({}): ", location.file_name(), location.line());
	fmt::vprint(stderr, format, args);
}

} // namespace logger
